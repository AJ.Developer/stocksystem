using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StockSystem.DataLayer
{
    [Table("Users")]
    public class User : IDataErrorInfo
    {
        public string this[string columnName]
        {
            get
            {
                if (columnName.Equals("Username") && string.IsNullOrEmpty(this.Username))
                    return "Username Is Required";
                return string.Empty;
            }
        }

        [Key]
        public long UserID { get; set; }

        [Required]
        public string Username { get; set; }
        public string Password { get; set; }
        public bool IsAdmin { get; set; }
        [NotMapped]
        public string Error => "";
    }
}
