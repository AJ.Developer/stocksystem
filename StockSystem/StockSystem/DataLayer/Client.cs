using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StockSystem.DataLayer
{
    [Table("Clients")]
    public partial class Client : IDataErrorInfo
    {
        [NotMapped]
        public string Error => "";
        public string this[string columnName]
        {
            get
            {
                if (columnName.Equals("Name") && string.IsNullOrEmpty(this.Name))
                    return "Name Is Required";
                return string.Empty;
            }
        }
        [Key]
        public long ClientID { get; set; }
        [Required]
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
    }
}
