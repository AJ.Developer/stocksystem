﻿
using StockSystem.DataLayer;
using StockSystem.ViewModels;
using StockSystem.Views.Login;
using System;
using System.Globalization;
using System.Linq;
using System.Windows;

namespace StockSystem
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        void App_Startup(object sender, StartupEventArgs e)
        {
            try
            {

                ShutdownMode = ShutdownMode.OnLastWindowClose;
                if (DateTime.Now.Year > 2020)
                    this.Shutdown();
                //ApplicationThemeHelper.ApplicationThemeName = Theme.HybridApp.Name;

                using (var context = new DBModel())
                {
                    var Users = context.Users.ToArray();
                 
                }
                // Create main application window, starting minimized if specified
               
                var login = new LoginCollectionView();
                LoginCollectionViewModel loginVM = LoginCollectionViewModel.Create();
                loginVM.LoginCompleted += (a, args) =>
                {
                    MainWindow main = new MainWindow();
                    main.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    main.WindowState = WindowState.Maximized;
                    main.Show();
                    login.Close();
                };

                login.DataContext = loginVM;
                login.ShowDialog();
                CultureInfo ci = new CultureInfo("ar-LB");
                System.Threading.Thread.CurrentThread.CurrentCulture = ci;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                Current.ShutdownMode = ShutdownMode.OnLastWindowClose;
                Current.Shutdown(0);
            }

        }

    }
}
