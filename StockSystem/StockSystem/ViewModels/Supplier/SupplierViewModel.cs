﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using DevExpress.Mvvm;
using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.ViewModel;
using StockSystem.DBModelDataModel;
using StockSystem.Common;
using StockSystem.DataLayer;

namespace StockSystem.ViewModels {

    /// <summary>
    /// Represents the single Supplier object view model.
    /// </summary>
    public partial class SupplierViewModel : SingleObjectViewModel<Supplier, long, IDBModelUnitOfWork> {

        /// <summary>
        /// Creates a new instance of SupplierViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static SupplierViewModel Create(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null) {
            return ViewModelSource.Create(() => new SupplierViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the SupplierViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the SupplierViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected SupplierViewModel(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.Suppliers, x => x.Name) {
                }



    }
}
