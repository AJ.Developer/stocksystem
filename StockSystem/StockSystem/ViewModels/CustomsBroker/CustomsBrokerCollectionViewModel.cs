﻿using System;
using System.Linq;
using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.ViewModel;
using StockSystem.DBModelDataModel;
using StockSystem.Common;
using StockSystem.DataLayer;

namespace StockSystem.ViewModels {

    /// <summary>
    /// Represents the CustomsBrokers collection view model.
    /// </summary>
    public partial class CustomsBrokerCollectionViewModel : CollectionViewModel<CustomsBroker, long, IDBModelUnitOfWork> {

        /// <summary>
        /// Creates a new instance of CustomsBrokerCollectionViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static CustomsBrokerCollectionViewModel Create(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null) {
            return ViewModelSource.Create(() => new CustomsBrokerCollectionViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the CustomsBrokerCollectionViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the CustomsBrokerCollectionViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected CustomsBrokerCollectionViewModel(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.CustomsBrokers) {
        }
    }
}