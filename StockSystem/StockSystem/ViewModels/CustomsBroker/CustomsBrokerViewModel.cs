﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using DevExpress.Mvvm;
using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.ViewModel;
using StockSystem.DBModelDataModel;
using StockSystem.Common;
using StockSystem.DataLayer;

namespace StockSystem.ViewModels {

    /// <summary>
    /// Represents the single CustomsBroker object view model.
    /// </summary>
    public partial class CustomsBrokerViewModel : SingleObjectViewModel<CustomsBroker, long, IDBModelUnitOfWork> {

        /// <summary>
        /// Creates a new instance of CustomsBrokerViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static CustomsBrokerViewModel Create(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null) {
            return ViewModelSource.Create(() => new CustomsBrokerViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the CustomsBrokerViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the CustomsBrokerViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected CustomsBrokerViewModel(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.CustomsBrokers, x => x.Name) {
                }



    }
}
