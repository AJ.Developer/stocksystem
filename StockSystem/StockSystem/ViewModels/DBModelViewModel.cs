﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using DevExpress.Mvvm;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.ViewModel;
using StockSystem.Localization;using StockSystem.DBModelDataModel;

namespace StockSystem.ViewModels {
    /// <summary>
    /// Represents the root POCO view model for the DBModel data model.
    /// </summary>
    public partial class DBModelViewModel : DocumentsViewModel<DBModelModuleDescription, IDBModelUnitOfWork> {

		const string TablesGroup = "Tables";

		const string ViewsGroup = "Views";
		INavigationService NavigationService { get { return this.GetService<INavigationService>(); } }
	
        /// <summary>
        /// Creates a new instance of DBModelViewModel as a POCO view model.
        /// </summary>
        public static DBModelViewModel Create() {
            return ViewModelSource.Create(() => new DBModelViewModel());
        }

		        static DBModelViewModel() {
            MetadataLocator.Default = MetadataLocator.Create().AddMetadata<DBModelMetadataProvider>();
        }
        /// <summary>
        /// Initializes a new instance of the DBModelViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the DBModelViewModel type without the POCO proxy factory.
        /// </summary>
        protected DBModelViewModel()
		    : base(UnitOfWorkSource.GetUnitOfWorkFactory()) {
        }

        protected override DBModelModuleDescription[] CreateModules() {
			return new DBModelModuleDescription[] {
                new DBModelModuleDescription(DBModelResources.ClientPlural, "ClientCollectionView", TablesGroup, GetPeekCollectionViewModelFactory(x => x.Clients)),
                new DBModelModuleDescription(DBModelResources.ConfigurationPlural, "ConfigurationCollectionView", TablesGroup, GetPeekCollectionViewModelFactory(x => x.Configurations)),
                new DBModelModuleDescription(DBModelResources.CustomsBrokerPlural, "CustomsBrokerCollectionView", TablesGroup, GetPeekCollectionViewModelFactory(x => x.CustomsBrokers)),
                new DBModelModuleDescription(DBModelResources.ShipperPlural, "ShipperCollectionView", TablesGroup, GetPeekCollectionViewModelFactory(x => x.Shippers)),
                new DBModelModuleDescription(DBModelResources.SupplierPlural, "SupplierCollectionView", TablesGroup, GetPeekCollectionViewModelFactory(x => x.Suppliers)),
                new DBModelModuleDescription(DBModelResources.UserPlural, "UserCollectionView", TablesGroup, GetPeekCollectionViewModelFactory(x => x.Users)),
			};
        }
                		protected override void OnActiveModuleChanged(DBModelModuleDescription oldModule) {
            if(ActiveModule != null && NavigationService != null) {
                NavigationService.ClearNavigationHistory();
            }
            base.OnActiveModuleChanged(oldModule);
        }
	}

    public partial class DBModelModuleDescription : ModuleDescription<DBModelModuleDescription> {
        public DBModelModuleDescription(string title, string documentType, string group, Func<DBModelModuleDescription, object> peekCollectionViewModelFactory = null)
            : base(title, documentType, group, peekCollectionViewModelFactory) {
        }
    }
}