﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using DevExpress.Mvvm;
using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.ViewModel;
using StockSystem.DBModelDataModel;
using StockSystem.Common;
using StockSystem.DataLayer;

namespace StockSystem.ViewModels {

    /// <summary>
    /// Represents the single Shipper object view model.
    /// </summary>
    public partial class ShipperViewModel : SingleObjectViewModel<Shipper, long, IDBModelUnitOfWork> {

        /// <summary>
        /// Creates a new instance of ShipperViewModel as a POCO view model.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        public static ShipperViewModel Create(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null) {
            return ViewModelSource.Create(() => new ShipperViewModel(unitOfWorkFactory));
        }

        /// <summary>
        /// Initializes a new instance of the ShipperViewModel class.
        /// This constructor is declared protected to avoid undesired instantiation of the ShipperViewModel type without the POCO proxy factory.
        /// </summary>
        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
        protected ShipperViewModel(IUnitOfWorkFactory<IDBModelUnitOfWork> unitOfWorkFactory = null)
            : base(unitOfWorkFactory ?? UnitOfWorkSource.GetUnitOfWorkFactory(), x => x.Shippers, x => x.Name) {
                }



    }
}
