﻿using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.DataModel.EF6;
using StockSystem.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;

namespace StockSystem.DBModelDataModel {

    /// <summary>
    /// A DBModelUnitOfWork instance that represents the run-time implementation of the IDBModelUnitOfWork interface.
    /// </summary>
    public class DBModelUnitOfWork : DbUnitOfWork<DBModel>, IDBModelUnitOfWork {

        public DBModelUnitOfWork(Func<DBModel> contextFactory)
            : base(contextFactory) {
        }

        IRepository<Client, long> IDBModelUnitOfWork.Clients {
            get { return GetRepository(x => x.Set<Client>(), (Client x) => x.ClientID); }
        }

        IRepository<Configuration, int> IDBModelUnitOfWork.Configurations {
            get { return GetRepository(x => x.Set<Configuration>(), (Configuration x) => x.ID); }
        }

        IRepository<CustomsBroker, long> IDBModelUnitOfWork.CustomsBrokers {
            get { return GetRepository(x => x.Set<CustomsBroker>(), (CustomsBroker x) => x.CustomsBrokerID); }
        }

        IRepository<Shipper, long> IDBModelUnitOfWork.Shippers {
            get { return GetRepository(x => x.Set<Shipper>(), (Shipper x) => x.ShipperID); }
        }

        IRepository<Supplier, long> IDBModelUnitOfWork.Suppliers {
            get { return GetRepository(x => x.Set<Supplier>(), (Supplier x) => x.SupplierID); }
        }

        IRepository<User, long> IDBModelUnitOfWork.Users {
            get { return GetRepository(x => x.Set<User>(), (User x) => x.UserID); }
        }
    }
}
