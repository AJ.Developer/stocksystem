﻿using DevExpress.Mvvm.DataModel;
using DevExpress.Mvvm.DataModel.DesignTime;
using StockSystem.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;

namespace StockSystem.DBModelDataModel {

    /// <summary>
    /// A DBModelDesignTimeUnitOfWork instance that represents the design-time implementation of the IDBModelUnitOfWork interface.
    /// </summary>
    public class DBModelDesignTimeUnitOfWork : DesignTimeUnitOfWork, IDBModelUnitOfWork {

        /// <summary>
        /// Initializes a new instance of the DBModelDesignTimeUnitOfWork class.
        /// </summary>
        public DBModelDesignTimeUnitOfWork() {
        }

        IRepository<Client, long> IDBModelUnitOfWork.Clients {
            get { return GetRepository((Client x) => x.ClientID); }
        }

        IRepository<Configuration, int> IDBModelUnitOfWork.Configurations {
            get { return GetRepository((Configuration x) => x.ID); }
        }

        IRepository<CustomsBroker, long> IDBModelUnitOfWork.CustomsBrokers {
            get { return GetRepository((CustomsBroker x) => x.CustomsBrokerID); }
        }

        IRepository<Shipper, long> IDBModelUnitOfWork.Shippers {
            get { return GetRepository((Shipper x) => x.ShipperID); }
        }

        IRepository<Supplier, long> IDBModelUnitOfWork.Suppliers {
            get { return GetRepository((Supplier x) => x.SupplierID); }
        }

        IRepository<User, long> IDBModelUnitOfWork.Users {
            get { return GetRepository((User x) => x.UserID); }
        }
    }
}
