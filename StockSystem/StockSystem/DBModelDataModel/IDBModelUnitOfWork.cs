﻿using DevExpress.Mvvm.DataModel;
using StockSystem.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;

namespace StockSystem.DBModelDataModel {

    /// <summary>
    /// IDBModelUnitOfWork extends the IUnitOfWork interface with repositories representing specific entities.
    /// </summary>
    public interface IDBModelUnitOfWork : IUnitOfWork {
        
        /// <summary>
        /// The Client entities repository.
        /// </summary>
		IRepository<Client, long> Clients { get; }
        
        /// <summary>
        /// The Configuration entities repository.
        /// </summary>
		IRepository<Configuration, int> Configurations { get; }
        
        /// <summary>
        /// The CustomsBroker entities repository.
        /// </summary>
		IRepository<CustomsBroker, long> CustomsBrokers { get; }
        
        /// <summary>
        /// The Shipper entities repository.
        /// </summary>
		IRepository<Shipper, long> Shippers { get; }
        
        /// <summary>
        /// The Supplier entities repository.
        /// </summary>
		IRepository<Supplier, long> Suppliers { get; }
        
        /// <summary>
        /// The User entities repository.
        /// </summary>
		IRepository<User, long> Users { get; }
    }
}
