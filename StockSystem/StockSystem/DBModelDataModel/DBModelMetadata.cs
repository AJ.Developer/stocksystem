﻿using DevExpress.Mvvm.DataAnnotations;
using StockSystem.DataLayer;
using StockSystem.Localization;

namespace StockSystem.DBModelDataModel {

    public class DBModelMetadataProvider {
		        public static void BuildMetadata(MetadataBuilder<Client> builder) {
			builder.DisplayName(DBModelResources.Client);
						builder.Property(x => x.ClientID).DisplayName(DBModelResources.Client_ClientID);
						builder.Property(x => x.Name).DisplayName(DBModelResources.Client_Name);
						builder.Property(x => x.Address).DisplayName(DBModelResources.Client_Address);
						builder.Property(x => x.Phone).DisplayName(DBModelResources.Client_Phone);
			        }
		        public static void BuildMetadata(MetadataBuilder<Configuration> builder) {
			builder.DisplayName(DBModelResources.Configuration);
						builder.Property(x => x.ID).DisplayName(DBModelResources.Configuration_ID);
						builder.Property(x => x.Key).DisplayName(DBModelResources.Configuration_Key);
						builder.Property(x => x.Value).DisplayName(DBModelResources.Configuration_Value);
			        }
		        public static void BuildMetadata(MetadataBuilder<CustomsBroker> builder) {
			builder.DisplayName(DBModelResources.CustomsBroker);
						builder.Property(x => x.CustomsBrokerID).DisplayName(DBModelResources.CustomsBroker_CustomsBrokerID);
						builder.Property(x => x.Name).DisplayName(DBModelResources.CustomsBroker_Name);
						builder.Property(x => x.Address).DisplayName(DBModelResources.CustomsBroker_Address);
						builder.Property(x => x.Phone).DisplayName(DBModelResources.CustomsBroker_Phone);
			        }
		        public static void BuildMetadata(MetadataBuilder<Shipper> builder) {
			builder.DisplayName(DBModelResources.Shipper);
						builder.Property(x => x.ShipperID).DisplayName(DBModelResources.Shipper_ShipperID);
						builder.Property(x => x.Name).DisplayName(DBModelResources.Shipper_Name);
						builder.Property(x => x.Address).DisplayName(DBModelResources.Shipper_Address);
						builder.Property(x => x.Phone).DisplayName(DBModelResources.Shipper_Phone);
			        }
		        public static void BuildMetadata(MetadataBuilder<Supplier> builder) {
			builder.DisplayName(DBModelResources.Supplier);
						builder.Property(x => x.SupplierID).DisplayName(DBModelResources.Supplier_SupplierID);
						builder.Property(x => x.Name).DisplayName(DBModelResources.Supplier_Name);
						builder.Property(x => x.Address).DisplayName(DBModelResources.Supplier_Address);
						builder.Property(x => x.Phone).DisplayName(DBModelResources.Supplier_Phone);
			        }
		        public static void BuildMetadata(MetadataBuilder<User> builder) {
			builder.DisplayName(DBModelResources.User);
						builder.Property(x => x.UserID).DisplayName(DBModelResources.User_UserID);
						builder.Property(x => x.Username).DisplayName(DBModelResources.User_Username);
						builder.Property(x => x.Password).DisplayName(DBModelResources.User_Password);
						builder.Property(x => x.IsAdmin).DisplayName(DBModelResources.User_IsAdmin);
			        }
		    }
}